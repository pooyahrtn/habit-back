type Stage = 'developement' | 'stage' | 'test';

export interface Enviroment {
  readonly jwtSecret: string;
  readonly stage: Stage;
  readonly dbName: string;
  readonly urlPrefix: string;
  readonly mongo_username: string;
  readonly mongo_password: string;
  readonly mongo_url: string;
  readonly maxDistanceDefault: number;
  readonly usersPageSize: number;
  readonly messageMaxLength: number;
  readonly redisUrl: string;
  readonly smsAPIkey: string;
  readonly storageUrl: string;
}

export const enviroment: Enviroment = {
  dbName: process.env.DBNAME,
  jwtSecret: process.env.JWT_SECRET,
  stage: process.env.STAGE as Stage,
  urlPrefix: process.env.URL,
  mongo_username: process.env.MONGO_INITDB_ROOT_USERNAME,
  mongo_password: process.env.MONGO_INITDB_ROOT_PASSWORD,
  mongo_url: process.env.MONGO_URL,
  maxDistanceDefault: 5000,
  usersPageSize: 10,
  messageMaxLength: 200,
  redisUrl: process.env.REDIS_URL,
  smsAPIkey: process.env.SMS_API_KEY,
  storageUrl: process.env.STORAGE_URL,
};

export const checkEnviromentValid = () => {
  Object.keys(enviroment).forEach(k => {
    if (!enviroment[k]) {
      throw new Error(`Enviroment variable ${k} is not defined`);
    }
  });
};
