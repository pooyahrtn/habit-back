import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as express from 'express';
import * as helmet from 'helmet';
import { join } from 'path';
import { HttpExceptionFilter } from './http-exception.filter';
import { checkEnviromentValid } from './enviroment';
import * as rateLimit from 'express-rate-limit';

async function bootstrap() {
  checkEnviromentValid();
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.use(helmet());
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 200, // limit each IP to 100 requests per windowMs
    }),
  );
  app.use('/public', express.static(join(__dirname, '..', 'public')));
  await app.listen(3000);
}
bootstrap();
