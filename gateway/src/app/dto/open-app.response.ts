import { Field, Float, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class OpenApp {
  @Field(t => Float)
  androidMinVersion: number;
}
