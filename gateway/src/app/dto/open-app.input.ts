import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class OpenAppInput {
  @Field()
  fcmToken?: string;

  @Field({ nullable: true })
  appVersion?: number;
}
