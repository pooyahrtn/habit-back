import { Injectable } from '@nestjs/common';
import { OpenAppInput } from './dto/open-app.input';
import { UserDocumentType } from '../users/models/user.model';
import { UsersService } from '../users/users.service';
import { OpenApp } from './dto/open-app.response';
import { appConfig } from './app.config';

@Injectable()
export class AppService {
  constructor(private readonly userService: UsersService) {}

  async openApp(input: OpenAppInput, user: UserDocumentType): Promise<OpenApp> {
    const { fcmToken, appVersion } = input;
    if (fcmToken) {
      user.settings.fcmToken = fcmToken;
    }
    if (appVersion) {
      user.settings.appVersion = appVersion;
    }
    await user.save();
    return appConfig;
  }
}
