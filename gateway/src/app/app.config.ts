import { OpenApp } from './dto/open-app.response';

export const appConfig: OpenApp = {
  androidMinVersion: 1,
};
