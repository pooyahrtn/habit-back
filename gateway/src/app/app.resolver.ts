import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { OpenApp } from './dto/open-app.response';
import { AuthGaurd } from '../auth/auth.gaurd';
import { OpenAppInput } from './dto/open-app.input';
import { AppService } from './app.service';
import { CurrentUser } from '../users/users.decorator';
import { UserDocumentType } from '../users/models/user.model';

@AuthGaurd()
@Resolver(of => OpenApp)
export class AppResolver {

  constructor(private readonly appService: AppService) { }

  @Mutation(r => OpenApp)
  openApp(@Args('input') input: OpenAppInput, @CurrentUser() user: UserDocumentType) {
    return this.appService.openApp(input, user);
  }
}
