import { v4 as uuid } from 'uuid';
import * as fs from 'fs';
import * as path from 'path';
import * as sharp from 'sharp';
import { ReadStream } from 'fs-capacitor';
import { PhotoSize } from './config';

const publicDirName = 'public';
const publicDirPath = `./${publicDirName}`;

async function storeFile(
  streamData: ReadStream,
  filePath: string,
): Promise<boolean> {
  return new Promise((resolve, reject) => {
    streamData
      .pipe(fs.createWriteStream(filePath, { autoClose: true }))
      .on('error', (error: any) => {
        reject(error);
      })
      .on('finish', () => {
        resolve(true);
      });
  });
}

function createFolderIfNotExists(dirPath: string) {
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
  }
}

export interface SaveImage {
  folderName: string;
  filename: string;
  streamData: ReadStream;
}
/**
 * This function store the image
 * @returns file path
 */
export async function saveImage({
  folderName,
  filename,
  streamData,
}: SaveImage): Promise<string> {
  // check if base folder is available
  createFolderIfNotExists(publicDirPath);
  const userFolderPath = `${publicDirPath}/${folderName}`;
  // check if the user folder avaiable
  createFolderIfNotExists(userFolderPath);

  const extention = path.extname(filename);
  const storedFileName = uuid();
  const filePath = `${userFolderPath}/${storedFileName}${extention}`;

  await storeFile(streamData, filePath);
  return filePath.slice(1);
}

const imageExtensions = new Set(['png', 'jpg', 'jpeg', 'svg']);

export function isImage(fileName: string): boolean {
  return imageExtensions.has(
    path
      .extname(fileName)
      .slice(1)
      .toLowerCase(),
  );
}

function getFileLocation(filePath: string) {
  return /(.+\/)(.+\/.+\..+)/.exec(filePath)[2];
}

export async function deleteFile(filePath: string): Promise<void> {
  const fileLocation = getFileLocation(filePath);
  return new Promise((res, rej) => {
    fs.unlink(`${publicDirPath}/${fileLocation}`, (e) => {
      e ? rej(e) : res();
    });
  });
}
