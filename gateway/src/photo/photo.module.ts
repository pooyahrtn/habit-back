import { Module } from '@nestjs/common';
import { PhotoService } from './photo.service';
import { PhotoResolver } from './photo.resolver';
import { registerTypegooseModel } from '../utils';
import { Photo } from './photo.model';
import { BullModule } from '@nestjs/bull';
import { redisConfig } from '../redis';
import { PhotoProcessor } from './photo.processor';

@Module({
  imports: [
    registerTypegooseModel(Photo),
    BullModule.registerQueue({
      name: 'photo',
      redis: redisConfig,
    }),
  ],
  providers: [PhotoService, PhotoResolver, PhotoProcessor],
  exports: [PhotoService],
})
export class PhotoModule { }
