import { Injectable, BadRequestException } from '@nestjs/common';
import { FileUpload } from 'graphql-upload';
import { Photo } from './photo.model';
import { deleteFile, saveImage } from './utils';
import { UserDocumentType } from '../users/models/user.model';
import { photoSizes } from './config';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { SavePhoto } from './photo.processor';

@Injectable()
export class PhotoService {

  constructor(
    @InjectModel(Photo) private readonly photoModel: ReturnModelType<typeof Photo>,
    @InjectQueue('photo') private readonly photoQueue: Queue<SavePhoto>,
  ) {

  }

  async storeImage(file: FileUpload, folderName: string): Promise<void> {
    const { createReadStream, filename } = await file;
    await this.photoQueue.add({ filename, folderName, readStream: createReadStream });
  }

  async storeImageSync(file: FileUpload): Promise<Photo> {
    const { createReadStream, filename } = await file;
    this.checkIfImage(filename);
    const saveData = {
      folderName: 'global',
      filename,
    };

    const svgImage = await saveImage({
      ...saveData,
      streamData: createReadStream(),
    });

    const photo: Photo = {
      svg: svgImage,
      avatar: true,
    };
    await this.photoModel.create(photo);
    return photo;
  }

  async saveImage(photo: Photo) {
    return await this.photoModel.create(photo);
  }

  checkIfImage(fileName: string) {
    // if (!isImage(fileName)) {
    //   throw new BadRequestException('Selected file is not an image');
    // }
  }

  async deleteImage(imageId: string) {
    const image = await this.photoModel.findById(imageId);
    const { small, large, medium } = image;
    await this.photoModel.deleteOne({ _id: imageId });
    await deleteFile(small);
    await deleteFile(large);
    await deleteFile(medium);
  }

  async getAvatarImages() {
    return await this.photoModel.find({ avatar: true });
  }
}
