export interface PhotoSize {
  width: number;
  height: number;
}

interface PhotoSizes {
  small: PhotoSize;
  medium: PhotoSize;
  large: PhotoSize;
}

export const photoSizes: PhotoSizes = {
  small: {
    width: 80,
    height: 130,
  },
  medium: {
    width: 160,
    height: 300,
  },
  large: {
    width: 600,
    height: 1100,
  },
};
