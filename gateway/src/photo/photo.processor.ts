import { Processor, Process, OnQueueError, OnQueueFailed } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { saveImage } from './utils';
import { FileUpload } from 'graphql-upload';
import { photoSizes } from './config';
import { SaveImage } from '../photo/utils';
import { PhotoService } from '../photo/photo.service';
import { ReadStream } from 'fs';

export interface SavePhoto {
  filename: string;
  readStream: () => ReadStream;
  folderName: string;
}

@Processor('photo')
export class PhotoProcessor {
  private readonly logger = new Logger(PhotoProcessor.name);

  constructor(
    private readonly photoService: PhotoService,
  ) { }

  @Process()
  async savePhoto(job: Job<SavePhoto>) {
    this.logger.debug(`start storing photo`);
    const {
      data: {
        folderName,
        filename,
        readStream,
      },
    } = job;

    const saveData = {
      filename,
      folderName,
    };

    // const smallImage = await saveImage({
    //   ...saveData,
    //   streamData: readStream(),
    //   size: photoSizes.small,
    // });
    // const mediumImage = await saveImage({
    //   ...saveData,
    //   streamData: readStream(),
    //   size: photoSizes.medium,
    // });
    const largeImage = await saveImage({
      ...saveData,
      streamData: readStream(),
      // size: photoSizes.large,
    });
    await this.photoService.saveImage({
      small: largeImage,
      medium: largeImage,
      large: largeImage,
      avatar: false,
    });
    this.logger.debug(`successfully stored image`);
  }

  @OnQueueError()
  onError(error: Error) {
    this.logger.error(error);
  }

  @OnQueueFailed()
  onFailed(job: Job, err: Error) {
    this.logger.error(`saving a photo failed failed ${err}`);
  }
}
