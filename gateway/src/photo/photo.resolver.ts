import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { Photo } from '../photo/photo.model';
import { AuthGaurd } from '../auth/auth.gaurd';
import { Role } from '../users/models/user.model';
import { PhotoService } from '../photo/photo.service';
import { GraphQLUpload, FileUpload } from 'graphql-upload';

@AuthGaurd()
@Resolver(of => Photo)
export class PhotoResolver {
  constructor(
    private readonly photoService: PhotoService,
  ) { }

  @AuthGaurd([Role.SUPER_ADMIN, Role.ADMIN])
  @Mutation(r => Boolean)
  async addGlobalPhoto(
    @Args({ name: 'file', type: () => GraphQLUpload }) file: FileUpload,
  ) {
    await this.photoService.storeImageSync(file);
    return true;
  }

  @AuthGaurd([Role.SUPER_ADMIN, Role.ADMIN])
  @Mutation(r => Boolean)
  async deleteImage(
    @Args({ name: 'imageId', type: () => String }) image: string,
  ) {
    await this.photoService.deleteImage(image);
    return true;
  }

  @Query(r => [Photo])
  async avatarImages() {
    return await this.photoService.getAvatarImages();
  }
}
