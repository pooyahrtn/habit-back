import { prop, DocumentType } from '@typegoose/typegoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
export class Photo {
  @Field(t => ID)
  id?: string;

  @Field({ nullable: true })
  @prop()
  large?: string;

  @Field({ nullable: true })
  @prop()
  medium?: string;

  @Field({ nullable: true })
  @prop()
  small?: string;

  @Field({ nullable: true })
  @prop()
  svg?: string;

  @prop()
  avatar: boolean;
}

export type PhotoDocument = DocumentType<Photo>;
