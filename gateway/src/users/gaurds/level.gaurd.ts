import { Injectable, ExecutionContext, SetMetadata, applyDecorators, UseGuards } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlAuthGuard } from '../../auth/gql.guard';
import { UserDocumentType } from '../models/user.model';

@Injectable()
class Gaurd extends GqlAuthGuard {
  constructor(
    private readonly reflector: Reflector,
  ) {
    super();
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const level = this.reflector.get<number>('level', context.getHandler());

    if (!level) {
      return true;
    }

    const user: UserDocumentType = this.getRequest(context).user;

    return user && user.game.level >= level;
  }
}

export const LevelGaurd = (minLevel: number) => applyDecorators(
  SetMetadata('level', minLevel),
  UseGuards(GqlAuthGuard, Gaurd),
);
