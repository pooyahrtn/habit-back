import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { User } from './models/user.model';
import { registerTypegooseModel } from '../utils';
import { FilestorageModule } from '../filestorage/filestorage.module';
import { ProfileResolver } from './profile.resolver';

@Module({
  imports: [
    registerTypegooseModel(User),
    FilestorageModule,
  ],
  providers: [UsersService, UsersResolver, ProfileResolver],
  exports: [UsersService],
})
export class UsersModule { }
