import { InputType, Field, Int } from '@nestjs/graphql';
import { MaxLength } from 'class-validator';
import { Sex } from '../models/profile.model';
import { Profile } from '../../users/models/profile.model';
import { Settings } from '../models/settings.model';

@InputType()
export class UpdateUserInput {
  @Field(t => Profile)
  profile: Profile;

  @Field(t => Settings)
  settings: Settings;
}
