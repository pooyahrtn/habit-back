export const validation = {
  name: {
    maxLength: 40,
    minLength: 1,
  },
};
