import { User } from './models/user.model';
import { Sex } from './models/profile.model';

export const pickRandomImage = (user: User): string => {
  const { profile: { sex } } = user;
  const imageNumber = Math.ceil(6 * Math.random());
  if (sex === Sex.Female) {
    return `f-y-${imageNumber}`;
  } else if (sex === Sex.Male) {
    return `m-y-${imageNumber}`;
  }
  const randomSex = Math.random() > 0.5;
  return `${randomSex ? 'm' : 'f'}-y-${imageNumber}`;
};

export const getImages = (): string[] => {
  const fe = 'f-y-';
  const ma = 'm-y-';
  const maleImages = [...Array(6).keys()].map(i => `${ma}${i + 1}`);
  const femaleImages = [...Array(6).keys()].map(i => `${fe}${i + 1}`);
  return [...maleImages, ...femaleImages];
};
