import {
  Resolver,
  Mutation,
  Query,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';

import { User, UserDocumentType } from './models/user.model';
import { UsersService } from './users.service';
import { CurrentUser } from './users.decorator';
import { AuthGaurd } from '../auth/auth.gaurd';
import { UpdateUserInput } from './dto/update-user.input';
import { Profile } from './models/profile.model';
import { FilestorageService } from '../filestorage/filestorage.service';
import { LeaderBoardUser } from './entities/LeaderBoardUser';

@Resolver(of => User)
export class UsersResolver {
  constructor(
    private readonly userService: UsersService,
    private readonly fileStorageService: FilestorageService,
  ) { }

  @AuthGaurd()
  @Query(returns => User)
  me(@CurrentUser() user: any): User {
    return user;
  }

  @AuthGaurd()
  @Mutation(returns => User)
  async updateMe(
    @CurrentUser() user: UserDocumentType,
    @Args('user') input: UpdateUserInput,
  ) {
    return await this.userService.updateProfile(user, input);
  }

  @AuthGaurd()
  @Query(r => [LeaderBoardUser], { name: 'leaderboard' })
  async getLeaderboard(): Promise<LeaderBoardUser[]> {
    return this.userService.getLeaderBoard();
  }

  @AuthGaurd()
  @Query(r => [String], { name: 'avatarImages' })
  getImages(): string[] {
    return this.userService.getProfileImages();
  }

}
