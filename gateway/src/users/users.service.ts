import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import {
  User,
  UserModelType,
  UserDocumentType,
  Role,
} from './models/user.model';
import { UpdateUserInput } from './dto/update-user.input';
import { Profile } from './models/profile.model';
import { Task } from '../task/models/task.model';
import { Challenge } from '../challenge/models/challenge.model';
import { pickRandomImage, getImages } from './users.functions';
import { addScore, getSignupScore } from '../game/game.functions';
import { LeaderBoardUser } from './entities/LeaderBoardUser';
import { FilestorageService } from '../filestorage/filestorage.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User) private readonly userModel: UserModelType,
    private readonly fileStorageService: FilestorageService,
  ) {}

  async getOrCreate(data: { phoneNumber: string }) {
    const { phoneNumber } = data;
    const existingUser = await this.userModel.findOne({ phoneNumber });
    if (existingUser) {
      return existingUser;
    }
    const score = getSignupScore();
    return await this.userModel.create({
      phoneNumber,
      game: addScore(score),
      tasks: [],
    } as User);
  }

  async findUserById(id: string, fields?: Array<keyof User>) {
    return await this.userModel.findById(id, fields);
  }

  async updateProfile(
    user: UserDocumentType,
    data: UpdateUserInput,
  ): Promise<User> {
    user.profile = {
      ...user.toObject().profile,
      ...data.profile,
    };
    if (!user.profile.image) {
      user.profile.image = pickRandomImage(user);
    }
    user.settings = {
      ...user.toObject().settings,
      ...data.settings,
    };
    await user.save();

    return user;
  }

  async addTask(user: UserDocumentType, task: Task) {
    user.tasks.push(task);
    return await user.save();
  }

  async addUserScore(user: UserDocumentType, score: number) {
    const lastLevel = user.game.level;
    user.game = addScore(score, user.game);
    await user.save();
    return {
      newLevel: lastLevel !== user.game.level,
    };
  }

  async getLeaderBoard(): Promise<LeaderBoardUser[]> {
    const topUsers = await this.userModel
      .find(
        {
          $and: [
            {
              $or: [{ blocked: false }, { blocked: undefined }],
            },
            {
              roles: { $nin: [Role.SUPER_ADMIN, Role.ADMIN] },
            },
            {
              profile: { $exists: true },
            },
          ],
        },
        ['profile', 'game'],
      )
      .sort('-game.score')
      .limit(20)
      .exec();
    return topUsers.map(u => ({ id: u.id, profile: u.profile, game: u.game }));
  }

  getProfileImages() {
    const images = getImages();
    const avatarPath = this.fileStorageService.avatarUrls();
    return images.map(i => avatarPath + i + '.png');
  }

  // async updateFcmToken(user: UserDocumentType, token: string) {
  //   user.settings.fcmToken = token;
  //   return await user.save();
  // }
}
