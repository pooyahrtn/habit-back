import {
  Resolver,
  ResolveField,
  Parent,
} from '@nestjs/graphql';

import { Profile } from './models/profile.model';
import { FilestorageService } from '../filestorage/filestorage.service';

@Resolver(of => Profile)
export class ProfileResolver {
  constructor(
    private readonly fileStorageService: FilestorageService,
  ) { }

  @ResolveField('image', r => String)
  getUserImage(@Parent() profile: Profile) {
    return this.fileStorageService.avatarUrls() + profile.image + '.png';
  }
}
