import { Field, ObjectType, InputType } from '@nestjs/graphql';
import { prop } from '@typegoose/typegoose';

@ObjectType('Settings')
@InputType('SettingsInput')
export class Settings {
  @Field()
  @prop()
  notifications: boolean;

  @prop({ required: false })
  fcmToken?: string;

  @prop({ required: false })
  appVersion?: number;
}
