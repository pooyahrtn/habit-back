import { Field, Int, Float, ObjectType } from '@nestjs/graphql';
import { prop } from '@typegoose/typegoose';

@ObjectType()
export class Game {
  @Field(type => Int)
  @prop({ default: 0, index: true })
  score: number;

  @Field(type => Int)
  @prop({ default: 1 })
  level: number;

  @Field(type => Float)
  @prop({ default: 0 })
  progress: number;
}
