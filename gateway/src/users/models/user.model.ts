import {
  prop,
  arrayProp,
  ReturnModelType,
  DocumentType,
  index,
  Ref,
} from '@typegoose/typegoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { Profile } from './profile.model';
import { Settings } from './settings.model';
import { Game } from './game.model';
import { Task } from '../../task/models/task.model';

export enum Role {
  ADMIN = 'ADMIN',
  USER = 'USER',
  SUPER_ADMIN = 'SUPER_ADMIN',
}

@ObjectType()
export class User {
  @Field(type => ID)
  id: string;

  @prop({ required: true })
  @Field()
  phoneNumber: string;

  @Field(type => [String], { nullable: true })
  @arrayProp({ enum: Role, items: String })
  roles?: Role[];

  @Field(type => Profile, { nullable: true })
  @prop({ _id: false })
  profile?: Profile;

  @Field(type => Game, { nullable: true })
  @prop({ _id: false })
  game?: Game;

  @Field(type => Settings, { nullable: true })
  @prop({ _id: false })
  settings?: Settings;

  @arrayProp({ itemsRef: Task.name })
  tasks: Array<Ref<Task>>;

  @prop({ default: false })
  blocked?: boolean;

}

export type UserModelType = ReturnModelType<typeof User>;
export type UserDocumentType = DocumentType<User>;
