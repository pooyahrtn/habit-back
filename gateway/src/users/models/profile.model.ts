import { ObjectType, Field, Int, InputType, registerEnumType } from '@nestjs/graphql';
import { prop } from '@typegoose/typegoose';
import { Photo } from '../../photo/photo.model';
import { MaxLength } from 'class-validator';

export enum Sex {
  Male = 'male',
  Female = 'female',
  Other = 'other',
}

registerEnumType(Sex, { name: 'Sex' });

@ObjectType()
@InputType('ProfileInput')
export class Profile {
  @Field()
  @prop({ maxlength: 40 })
  @MaxLength(40)
  username: string;

  @Field(type => Sex)
  @prop({ enum: Sex, default: Sex.Other })
  sex: Sex;

  @Field(type => Int)
  @prop({ max: 100, min: 0 })
  age: number;

  @Field({ nullable: true })
  @prop({ maxlength: 30 })
  image: string;

}
