import { Profile } from '../models/profile.model';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { Game } from '../models/game.model';

@ObjectType()
export class LeaderBoardUser {
  @Field(t => ID)
  id: string;

  @Field(t => Profile)
  profile: Profile;

  @Field(t => Game)
  game: Game;
}
