import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { Task } from './models/task.model';
import { AuthGaurd } from '../auth/auth.gaurd';
import { TaskService } from './task.service';
import { AcceptChallengeInput } from './dto/accept-challenge.input';
import { CurrentUser } from '../users/users.decorator';
import { DayTask } from './entities/day-task.entity';
import { UserDocumentType } from '../users/models/user.model';
import { RemoveChallengeInput } from './dto/remove-challenge.input';
import { DoTaskInput } from './dto/do-task.input';
import { TaskStatus } from './entities/task-status.entity';
import { DoTaskResponse } from './entities/do-task.response';

@AuthGaurd()
@Resolver(of => Task)
export class TaskResolver {
  constructor(
    private readonly taskService: TaskService,
  ) { }

  @Mutation(r => [DayTask])
  async acceptChallenge(
    @Args('acceptChallengeInput') input: AcceptChallengeInput,
    @CurrentUser() user,
  ): Promise<DayTask[]> {
    return await this.taskService.acceptChallenge(input, user);
  }

  @Query(r => [DayTask], { name: 'mySchedule' })
  async getMySchedule(
    @CurrentUser() user: UserDocumentType,
  ) {
    return await this.taskService.getUserSchedule(user);
  }

  @Mutation(r => [DayTask])
  async cancelChallenge(
    @CurrentUser() user,
    @Args('cancelInput') input: RemoveChallengeInput) {
    await this.taskService.cancelChallenge(user, input.challengeId);
    return await this.taskService.getUserSchedule(user);
  }

  @Mutation(r => DoTaskResponse)
  async doTask(
    @CurrentUser() user,
    @Args('doTaskInput') input: DoTaskInput,
  ): Promise<DoTaskResponse> {
    return await this.taskService.doATask(user, input);
  }

  @Query(r => Boolean, { name: 'canAcceptMore' })
  getCanUserAcceptMore(@CurrentUser() user: UserDocumentType): Promise<boolean> {
    return this.taskService.canUserAcceptMoreChallenges(user);
  }
}
