import { prop } from '@typegoose/typegoose';
import { Day } from '../../utils';

export class Action {
  @prop({ required: true })
  reward: number;

  @prop({ enum: Day })
  day: Day;

  @prop({ default: Date.now() })
  createdAt: Date;
}
