import { ObjectType, Field, Int, registerEnumType, ID } from '@nestjs/graphql';
import {
  Ref,
  prop,
  arrayProp,
  ReturnModelType,
  DocumentType,
} from '@typegoose/typegoose';
import { Challenge } from '../../challenge/models/challenge.model';
import { Action } from './action.model';
import { Day } from '../../utils';
import { Types } from 'mongoose';

registerEnumType(Day, { name: 'Day' });

@ObjectType()
export class Task {
  @Field(t => ID)
  id?: string;

  @Field(t => Challenge)
  @prop({ ref: Challenge, required: true })
  challenge: Ref<Challenge>;

  @prop({ ref: 'User' })
  user: Types.ObjectId;

  @prop({ default: true })
  active: boolean;

  @arrayProp({ items: Action })
  actions: Action[];

  @arrayProp({ enum: Day, items: String })
  days: Day[];

  @prop({ required: false })
  remindTime?: Date;
}

export type TaskModel = ReturnModelType<typeof Task>;
export type TaskDocument = DocumentType<Task>;
