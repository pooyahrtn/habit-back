import { Day } from '../../utils';
import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class DoTaskInput {
  @Field()
  taskId: string;

  @Field(t => Day)
  day: Day;
}
