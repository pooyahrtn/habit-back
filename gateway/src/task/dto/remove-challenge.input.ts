import { InputType, Field } from '@nestjs/graphql';


@InputType()
export class RemoveChallengeInput {
  @Field()
  challengeId: string;

}
