import { InputType, Field } from '@nestjs/graphql';
import { Day } from '../../utils/time';

@InputType()
export class AcceptChallengeInput {
  @Field()
  challengeId: string;

  @Field(t => [Day], { nullable: 'itemsAndList' })
  days: Day[];

  @Field(t => Date, { nullable: true })
  remindTime?: Date;
}
