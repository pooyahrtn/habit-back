import { DayTask } from './entities/day-task.entity';
import { Task } from './models/task.model';
import { Day } from '../utils';
import { TaskStatus } from './entities/task-status.entity';
import { getChallengeReward } from '../game/game.functions';
import moment = require('moment');
import { Challenge } from '../challenge/models/challenge.model';
import { Action } from './models/action.model';

type GetMySchedule = (tasks: Task[]) => DayTask[];

export const getMySchedule: GetMySchedule = tasks => {
  const days = Object.values(Day);
  const groupedByDay = days.map(d => ({
    day: d,
    tasks: getDayTasks(tasks, d),
  }));
  return groupedByDay.map(i => ({
    day: i.day,
    taskStatuses: i.tasks.map(t => getTaskStatus(t, i.day)),
  }));
};

const taskHasDay = (task: Task, day: Day) => task.days.includes(day);

const getDayTasks = (tasks: Task[], day: Day) =>
  tasks.filter(t => taskHasDay(t, day));

export const getTaskStatus = (task: Task, day: Day): TaskStatus => ({
  id: `${day}${task.id}`,
  taskId: task.id,
  challenge: task.challenge as any,
  isDone: isTaskDoneToday(task, day),
  reward: getChallengeReward(task.challenge as Challenge).step,
  nCompleted: task.actions.length,
  isCompleted: isTaskCompleted(task),
  remindTime: task.remindTime,
});

export const isTaskCompleted = (task: Task) => {
  const challenge: Challenge = task.challenge as Challenge;
  return task.actions.length === challenge.repeatCount;
};

export const isTaskDoneToday = (task: Task, todayDay: Day): boolean => {
  if (task.actions.length === 0) {
    return false;
  }
  const lastAction = task.actions[task.actions.length - 1];
  if (lastAction.day !== todayDay) {
    return false;
  }
  return moment(lastAction.createdAt).diff(moment(), 'days') < 1;
};

export const getDoneTaskAction = (task: Task, day: Day): Action => {
  return {
    day,
    reward: getChallengeReward(task.challenge as Challenge).step,
    createdAt: new Date(),
  };
};
