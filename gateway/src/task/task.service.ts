import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { Task, TaskModel } from './models/task.model';
import { UserDocumentType, User } from '../users/models/user.model';
import { UsersService } from '../users/users.service';
import { Types, FilterQuery } from 'mongoose';
import { ToastExeption } from '../exeptions';
import { DayTask } from './entities/day-task.entity';
import { getMySchedule, getDoneTaskAction } from './task.functions';
import { AcceptChallengeInput } from './dto/accept-challenge.input';
import { ChallengeDocuement } from '../challenge/models/challenge.model';
import { ChallengeService } from '../challenge/challenge.service';
import { Day } from '../utils';
import { DoTaskInput } from './dto/do-task.input';
import { TaskStatus } from './entities/task-status.entity';
import {
  getTaskStatus,
  isTaskDoneToday,
  isTaskCompleted,
} from './task.functions';
import { DoTaskResponse } from './entities/do-task.response';
import {
  getChallengeReward,
  canAcceptMoreChallenge,
} from '../game/game.functions';
import { Challenge } from '../challenge/models/challenge.model';

@Injectable()
export class TaskService {
  constructor(
    @InjectModel(Task) private readonly taskModel: TaskModel,
    private readonly userService: UsersService,
    private readonly challengeService: ChallengeService,
  ) {}

  async acceptChallenge(
    data: AcceptChallengeInput,
    user: UserDocumentType,
  ): Promise<DayTask[]> {
    const { challengeId, days, remindTime } = data;

    const userTasks = await this.getUserTasks(user);
    const challenge = await this.challengeService.findChallengeById(
      challengeId,
    );

    // check if user can accept more challenges
    if (!(await this.canUserAcceptMoreChallenges(user))) {
      throw new ToastExeption('You can not accept more challenges');
    }

    // check if user has enrolled challenge
    if (await this.hasEnrolledChallenge(userTasks, challenge)) {
      throw new ToastExeption('You have already accepted this challenge');
    }
    // create the task
    const task = new this.taskModel({
      challenge,
      active: true,
      actions: [],
      user: user._id,
      remindTime,
      days: getSelectedDays(days, challenge),
    } as Task);
    await task.save();

    await this.userService.addTask(user, task);

    challenge.stats.enrolledCount += 1;
    await challenge.save();

    return this.getUserSchedule(user);
  }

  async getUserTasks(user: UserDocumentType, query: FilterQuery<Task> = {}) {
    const userTaskIds = user.tasks as Types.ObjectId[];
    return await this.taskModel
      .find({ _id: { $in: userTaskIds }, active: true, ...query })
      .populate('challenge' as keyof Task);
  }

  async hasEnrolledChallenge(userTasks: Task[], challenge: ChallengeDocuement) {
    return userTasks.some(t => {
      if (!t.active) {
        return true;
      }
      return challenge._id.equals((t.challenge as any)._id);
    });
  }

  async getUserSchedule(user: UserDocumentType): Promise<DayTask[]> {
    const userTasks = await this.getUserTasks(user);
    const res = getMySchedule(userTasks);
    return res;
  }

  async cancelChallenge(user: UserDocumentType, challengeId: string) {
    const userTasks = await this.getUserTasks(user);
    const selectedTask = userTasks.find((t: any) =>
      t.challenge._id.equals(challengeId),
    );
    if (selectedTask) {
      selectedTask.active = false;
      await selectedTask.save();
    }
  }

  async doATask(
    user: UserDocumentType,
    data: DoTaskInput,
  ): Promise<DoTaskResponse> {
    const { taskId, day } = data;
    const task = await this.taskModel
      .findOne({ _id: taskId, user: user._id })
      .populate('challenge');
    const challenge: ChallengeDocuement = task.challenge as ChallengeDocuement;
    if (isTaskDoneToday(task, day)) {
      throw new ToastExeption('Task is already done');
    }
    const newAction = getDoneTaskAction(task, day);
    task.actions.push(newAction);
    const completed = isTaskCompleted(task);

    const challengeReward = getChallengeReward(challenge as Challenge);
    const reward = completed ? challengeReward.complete : challengeReward.step;

    // update user score
    const { newLevel } = await this.userService.addUserScore(user, reward);

    // set task in active if it's completed:
    if (completed) {
      task.active = false;
      challenge.stats.successCount += 1;
      await challenge.save();
    }

    await task.save();
    return {
      reward,
      completed,
      newLevel,
      challengeName: challenge.name,
      challengeId: challenge.id,
    };
  }

  async canUserAcceptMoreChallenges(user: UserDocumentType): Promise<boolean> {
    const userTasks = await this.getUserTasks(user);
    const userChallenges = userTasks.map(t => t.challenge as Challenge);
    return canAcceptMoreChallenge(userChallenges, user.game.level);
  }
}

const getSelectedDays = (days: Day[], challenge: ChallengeDocuement) => {
  if (challenge.repeatCount === 7 || !days) {
    return Object.values(Day);
  }
  return days;
};
