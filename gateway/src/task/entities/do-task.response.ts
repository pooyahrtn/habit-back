import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class DoTaskResponse {
  @Field()
  reward: number;

  @Field()
  completed: boolean;

  @Field()
  newLevel: boolean;

  @Field()
  challengeName: string;

  @Field()
  challengeId: string;
}
