// It's the response of home page, which show user their daily tasks.

import { Day } from '../../utils';
import { Field, ObjectType } from '@nestjs/graphql';
import { TaskStatus } from './task-status.entity';

@ObjectType()
export class DayTask {
  @Field(t => Day)
  day: Day;

  @Field(t => [TaskStatus])
  taskStatuses: TaskStatus[];
}
