import { ObjectType, Field, Int, ID } from '@nestjs/graphql';
import { Challenge } from '../../challenge/models/challenge.model';

@ObjectType()
export class TaskStatus {
  @Field(t => ID)
  id: string;

  @Field()
  taskId: string;

  @Field(t => Challenge)
  challenge: Challenge;

  @Field()
  isDone: boolean;

  @Field(t => Int)
  reward: number;

  @Field(t => Int)
  nCompleted: number;

  @Field()
  isCompleted: boolean;

  @Field(t => Date, { nullable: true })
  remindTime?: Date;
}
