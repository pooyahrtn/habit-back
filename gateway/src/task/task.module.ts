import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskResolver } from './task.resolver';
import { registerTypegooseModel } from '../utils';
import { Task } from './models/task.model';
import { UsersModule } from '../users/users.module';
import { ChallengeModule } from '../challenge/challenge.module';

@Module({
  imports: [
    registerTypegooseModel(Task),
    UsersModule,
    ChallengeModule,
  ],
  providers: [TaskService, TaskResolver],
  exports: [TaskService],
})
export class TaskModule { }
