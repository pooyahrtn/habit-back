import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypegooseModule } from 'nestjs-typegoose';
import { enviroment } from './enviroment';
import { SmsModule } from './sms/sms.module';
import { PhotoModule } from './photo/photo.module';
import { UsersModule } from './users/users.module';
import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { ChallengeModule } from './challenge/challenge.module';
import { TaskModule } from './task/task.module';
import { FilestorageModule } from './filestorage/filestorage.module';
import { GameModule } from './game/game.module';
import { AppModule as ApplicationModule } from './app/app.module';
import { FirebaseModule } from './firebase/firebase.module';

export const AppGraphQLModule = GraphQLModule.forRoot({
  autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
  debug: enviroment.stage === 'developement',
  tracing: enviroment.stage === 'developement',
  playground: enviroment.stage === 'developement',
  context: ({ req }) => ({ req }),
  uploads: {
    maxFileSize: 10000000, // 10 MB
    maxFiles: 6,
  },
});

const MongoConnection = TypegooseModule.forRoot(
  `mongodb://${enviroment.mongo_username}:${enviroment.mongo_password}@${enviroment.mongo_url}:27017/${enviroment.dbName}?authSource=admin`,
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true } as any,
);

@Module({
  imports: [
    AppGraphQLModule,
    MongoConnection,
    PhotoModule,
    SmsModule,
    UsersModule,
    AuthModule,
    ChallengeModule,
    TaskModule,
    FilestorageModule,
    GameModule,
    ApplicationModule,
    FirebaseModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
