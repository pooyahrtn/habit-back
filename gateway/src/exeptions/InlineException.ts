import { HttpException, HttpStatus } from '@nestjs/common';

export class InlineExeption extends HttpException {
  constructor(message: string) {
    super(
      {
        type: 'inline',
        message,
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}
