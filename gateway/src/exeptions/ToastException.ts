import { HttpException, HttpStatus } from '@nestjs/common';

export class ToastExeption extends HttpException {
  constructor(message: string) {
    super(
      {
        type: 'toast',
        message,
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}
