import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@Injectable()
export class SmsService {
  constructor(@InjectQueue('sms') private readonly smsQueue: Queue) { }
  async sendSms(phoneNumber: string, confirmCode: string) {
    await this.smsQueue.add({ phoneNumber, confirmCode }, { priority: 1, attempts: 3 });
  }
}
