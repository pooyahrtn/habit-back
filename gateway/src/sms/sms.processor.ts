import { Process, Processor, OnQueueError, OnQueueFailed } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import axios from 'axios';
import { enviroment } from '../enviroment';

const kavenegarURL = `https://api.kavenegar.com/v1/${enviroment.smsAPIkey}/verify/lookup.json`;
const requestSend = (phone: string, code: string) =>
  axios.get(kavenegarURL, {
    params: {
      receptor: `+${phone}`,
      token: code,
      template: 'yadetnareverify',
    },
  });

@Processor('sms')
export class SmsProcessor {
  private readonly logger = new Logger(SmsProcessor.name);

  @Process()
  async handleSendMessage(job: Job) {
    this.logger.debug('start request sending message');
    const { data: { phoneNumber, confirmCode } } = job;
    const res = await requestSend(phoneNumber, confirmCode);
    this.logger.debug(`sms sent ${res.data}`);
  }

  @OnQueueError()
  onError(error: Error) {
    this.logger.error(error);
  }

  @OnQueueFailed()
  onFailed(job: Job, err: Error) {
    this.logger.error(`sending ${job.data.phoneNumber} failed ${err}`);
  }
}
