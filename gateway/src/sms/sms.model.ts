import {
  prop,
  arrayProp,
  ReturnModelType,
  DocumentType,
  index,
  Ref,
} from '@typegoose/typegoose';
import { ObjectType } from '@nestjs/graphql';

@ObjectType()
export class SMS {

}

export type SMSModel = ReturnModelType<typeof SMS>;
export type SMSDocument = DocumentType<SMS>;
