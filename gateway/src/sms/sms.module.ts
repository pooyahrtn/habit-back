import { Module } from '@nestjs/common';
import { SmsService } from './sms.service';
import { registerTypegooseModel } from '../utils';
import { SMS } from './sms.model';
import { BullModule } from '@nestjs/bull';
import { redisConfig } from '../redis';
import { SmsProcessor } from './sms.processor';

@Module({
  imports: [
    registerTypegooseModel(SMS),
    BullModule.registerQueue({
      name: 'sms',
      redis: redisConfig,
    }),
  ],
  providers: [SmsService, SmsProcessor],
  exports: [SmsService],
})
export class SmsModule { }
