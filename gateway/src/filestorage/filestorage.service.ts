import { Injectable } from '@nestjs/common';
import { enviroment } from '../enviroment';

@Injectable()
export class FilestorageService {
  challengeUrl() {
    return `${enviroment.storageUrl}/global/challenge/`;
  }
  avatarUrls() {
    return `${enviroment.storageUrl}/global/profile/`;
  }
}
