import { Injectable, Logger } from '@nestjs/common';
import { Phone, PhoneModel } from './phone.model';
import { InjectModel } from 'nestjs-typegoose';
import {
  generateConfirmCode,
  canSendConfirmCode,
  transformPhoneAfterSend,
  isConfirmCodeFresh,
} from './utils';
import { SmsService } from '../sms/sms.service';
import { InlineExeption } from '../exeptions';
import { authLocale } from './auth.locale';
import { Auth } from './auth.entity';
import { AuthPayload } from './auth.payload';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  private logger = new Logger(AuthService.name);

  constructor(
    @InjectModel(Phone) private readonly phoneModel: PhoneModel,
    private readonly smsService: SmsService,
    private readonly jwtService: JwtService,
    private readonly userService: UsersService,
  ) { }

  // TODO: refactor it
  async createPhoneAuth(phoneNumber: string): Promise<Phone> {
    const existingPhone = await this.phoneModel.findOne({ phoneNumber });
    // User has tried before
    if (existingPhone) {
      if (!canSendConfirmCode(existingPhone)) {
        throw new InlineExeption(authLocale.tryAgainLater);
      } else {
        // let's send confirm code agains
        transformPhoneAfterSend(existingPhone);
        await this.smsService.sendSms(phoneNumber, existingPhone.confirmCode);
        await existingPhone.save();
        return existingPhone;
      }
    }
    const phoneObject = new this.phoneModel({
      phoneNumber,
      lastSentDate: new Date(),
      confirmCode: generateConfirmCode(),
      nTries: 1,
    } as Phone);
    await this.smsService.sendSms(phoneNumber, phoneObject.confirmCode);
    await phoneObject.save();
    return phoneObject;
  }

  async validateConfirmCode(phoneNumber: string, confirmCode: string): Promise<boolean> {
    const phoneObject = await this.phoneModel.findOne({ phoneNumber });
    if (!phoneObject) {
      this.logger.debug('phoneObject not found');
      return false;
    }
    if (!isConfirmCodeFresh(phoneObject)) {
      this.logger.debug('phoneObject is old');
      return false;
    }
    return phoneObject.confirmCode === confirmCode;
  }

  async verifyPhone(phoneNumber: string, confirmCode: string): Promise<Auth> {
    const isConfirmCodeValid = await this.validateConfirmCode(phoneNumber, confirmCode);
    if (!isConfirmCodeValid) {
      throw new InlineExeption(authLocale.invalidConfirmCode);
    }
    const user = await this.userService.getOrCreate({ phoneNumber });
    return this.createJwtTokens(user.id);
  }
  private createJwtTokens(userId: string): Auth {
    const payload: AuthPayload = { userId };
    return {
      accessToken: this.jwtService.sign(payload),
      refreshToken: this.jwtService.sign(payload, { expiresIn: '30d' }),
    };
  }
  async refreshAccessToken(refreshToken: string): Promise<Auth> {
    const payload = await this.jwtService.verifyAsync<AuthPayload>(
      refreshToken,
    );
    const user = await this.userService.findUserById(payload.userId);
    return this.createJwtTokens(user.id);
  }
}
