import { prop, ReturnModelType, DocumentType } from '@typegoose/typegoose';
import * as moment from 'moment';
import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class Phone {
  @Field(type => String)
  @prop({ required: true })
  phoneNumber: string;

  @Field(type => Date)
  @prop({ required: true })
  lastSentDate: Date;

  @prop({ required: true })
  confirmCode: string;

  @prop({ default: 0 })
  nTries: number;

  @Field(type => Date)
  get nextTry(): Date {
    return moment(this.lastSentDate).add(2, 'minutes').toDate();
  }
}

export type PhoneModel = ReturnModelType<typeof Phone>;
export type PhoneDocument = DocumentType<Phone>;
