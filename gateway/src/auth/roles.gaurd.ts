import { Injectable, ExecutionContext } from '@nestjs/common';

import { Reflector } from '@nestjs/core';

import { GqlAuthGuard } from './gql.guard';

@Injectable()
export class RolesGuard extends GqlAuthGuard {
  constructor(
    private readonly reflector: Reflector,
  ) {
    super();
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!roles) {
      return true;
    }

    const isValidUser = await super.canActivate(context);
    if (!isValidUser) {
      return false;
    }

    const user = this.getRequest(context).user;

    const hasRole = () => user.roles.some(role => roles.includes(role));

    return user && user.roles && hasRole();
  }
}
