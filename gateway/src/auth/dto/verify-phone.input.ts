import { InputType, Field } from '@nestjs/graphql';
import { Matches, MaxLength } from 'class-validator';

@InputType()
export class VerifyPhoneInput {
  @Field()
  @Matches(/989\d{9}/i)
  phoneNumber: string;

  @Field()
  @MaxLength(5)
  confirmCode: string;
}
