import { InputType, Field } from '@nestjs/graphql';
import { Matches } from 'class-validator';

@InputType()
export class CreateUserInput {
  @Field()
  @Matches(/989\d{9}/i)
  phoneNumber: string;
}
