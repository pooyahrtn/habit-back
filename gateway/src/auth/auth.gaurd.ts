import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { Role } from '../users/models/user.model';
import { RolesGuard } from './roles.gaurd';
import { GqlAuthGuard } from './gql.guard';

export const AuthGaurd = (roles?: Role[]) => applyDecorators(
  SetMetadata('roles', roles),
  UseGuards(GqlAuthGuard, RolesGuard),
);
