
import {
  Resolver,
  Mutation,
  Args,
} from '@nestjs/graphql';
import { Phone } from '../auth/phone.model';
import { Auth } from './auth.entity';
import { AuthService } from './auth.service';
import { CreateUserInput } from './dto/create-user.input';
import { VerifyPhoneInput } from './dto/verify-phone.input';

@Resolver(of => Auth)
export class AuthResolver {
  constructor(private readonly authService: AuthService) { }

  @Mutation(returns => Phone)
  createUser(@Args('input') input: CreateUserInput): Promise<Phone> {
    return this.authService.createPhoneAuth(input.phoneNumber);
  }

  @Mutation(returns => Auth)
  async verifyPhone(@Args('input') input: VerifyPhoneInput) {
    return this.authService.verifyPhone(input.phoneNumber, input.confirmCode);
  }

  @Mutation(returns => Auth)
  async refreshAccessToken(
    @Args({ name: 'refreshToken', type: () => String }) refreshToken: string,
  ) {
    return await this.authService.refreshAccessToken(refreshToken);
  }
}
