import { Phone } from './phone.model';
import moment = require('moment');
import { User } from '../users/models/user.model';
import { Role } from '../users/models/user.model';

function getRandomArbitrary(min: number, max: number) {
  return Math.random() * (max - min) + min;
}

export const generateConfirmCode = (): string => {
  return `${Math.round(getRandomArbitrary(10000, 99999))}`;
};

export const canSendConfirmCode = (phone: Phone) => {
  return moment(moment()).diff(phone.lastSentDate, 'minutes') > 2;
};

export const isConfirmCodeFresh = (phone: Phone) => {
  return moment(moment()).diff(phone.lastSentDate, 'minutes') < 2;
};

export const transformPhoneAfterSend = (phone: Phone): Phone => {
  // but functions should be immutable...
  phone.confirmCode = generateConfirmCode();
  phone.lastSentDate = new Date();
  phone.nTries += 1;
  return phone;
};

export const hasRole = (user: User, roles: Role[]) => user.roles.some(role => roles.includes(role));
