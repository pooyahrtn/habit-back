export const authLocale = {
  tryAgainLater: 'لطفا بعد از ۲ دقیقه مجددا تلاش کنید.',
  invalidConfirmCode: 'کد  اعتبارسنجی صحیح نیست',
};
