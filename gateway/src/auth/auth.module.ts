import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { registerTypegooseModel } from '../utils';
import { Phone } from './phone.model';
import { SmsModule } from '../sms/sms.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt-auth.strategy';
import { enviroment } from '../enviroment';
import { AuthResolver } from './auth.resolver';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    registerTypegooseModel(Phone),
    SmsModule,
    UsersModule,
    JwtModule.register({
      secret: enviroment.jwtSecret,
      signOptions: { expiresIn: '2d' },
    }),
  ],
  providers: [AuthService, JwtStrategy, AuthResolver],
  exports: [AuthService],
})
export class AuthModule { }
