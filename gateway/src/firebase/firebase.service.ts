import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { SendNotificationInput } from './dto/SendNotificationInput';

@Injectable()
export class FirebaseService {
  constructor(
    @InjectQueue('notification') private readonly notificationQueue: Queue,
  ) {}

  sendNotification(input: SendNotificationInput) {
    return this.notificationQueue.add(input, { attempts: 3 });
  }
}
