import { Process, Processor, OnQueueError, OnQueueFailed } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { FirebaseAdmin } from './firebase.instance';
import { SendNotificationInput } from './dto/SendNotificationInput';
@Processor('notification')
export class NotificationProcessor {
  private readonly logger = new Logger(NotificationProcessor.name);

  @Process()
  async handleSendMessage(job: Job<SendNotificationInput>) {
    this.logger.debug('start notification');
    const {
      data: {
        userToken,
        notification: { title, body },
      },
    } = job;
    FirebaseAdmin.messaging().sendToDevice(userToken, {
      notification: {
        title,
        body,
        tag: 'challenge_confirmed',
      },
    });

    this.logger.debug(`end sent notificatioon`);
  }

  @OnQueueError()
  onError(error: Error) {
    this.logger.error(error);
  }

  @OnQueueFailed()
  onFailed(job: Job<SendNotificationInput>, err: Error) {
    this.logger.error(`notification ${job.data.userToken} failed ${err}`);
  }
}
