import {
  prop,
  arrayProp,
  ReturnModelType,
  DocumentType,
  index,
  Ref,
} from '@typegoose/typegoose';
import { ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Notification {}

export type SMSModel = ReturnModelType<typeof Notification>;
export type SMSDocument = DocumentType<Notification>;
