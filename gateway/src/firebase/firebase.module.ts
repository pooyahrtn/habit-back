import { Module } from '@nestjs/common';
import { FirebaseService } from './firebase.service';
import { BullModule } from '@nestjs/bull';
import { redisConfig } from '../redis';
import { NotificationProcessor } from './notification.processor';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'notification',
      redis: redisConfig,
    }),
  ],
  providers: [FirebaseService, NotificationProcessor],
  exports: [FirebaseService],
})
export class FirebaseModule {}
