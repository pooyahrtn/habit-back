export interface SendNotificationInput {
  userToken: string;
  notification: {
    title: string;
    body: string;
  };
}
