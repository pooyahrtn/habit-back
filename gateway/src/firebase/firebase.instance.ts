import * as FirebaseAdmin from 'firebase-admin';
// tslint:disable-next-line: no-var-requires
const certs = require('../../serviceaccount.json');

FirebaseAdmin.initializeApp({
  credential: FirebaseAdmin.credential.cert(certs),
  databaseURL: 'https://hapit-a145e.firebaseio.com',
});

export { FirebaseAdmin };
