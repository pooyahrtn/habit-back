import { ChallengeReward } from '../challenge/interfaces/ChallengeReward';
import { Toughness } from '../challenge/models/toughness.enum';
import { Challenge } from '../challenge/models/challenge.model';
import { Game } from '../users/models/game.model';

export const getChallengeReward = (challenge: Challenge): ChallengeReward => {
  switch (challenge.toughness) {
    case Toughness.Easy:
      return {
        step: 3,
        complete: 15,
      };
    case Toughness.Medium:
      return {
        step: 5,
        complete: 25,
      };
    case Toughness.Hard:
      return {
        step: 10,
        complete: 50,
      };
  }
};

export const getSignupScore = () => 20;

const LEVEL_SCORES = [
  0,
  100,
  200,
  300,
  500,
  800,
  1300,
  2100,
  3400,
  5500,
  8900,
  15400,
  350000,
  700000,
];

export const getLevelMinScore = (level: number): number => {
  return LEVEL_SCORES[level - 1];
};

export const getCurrentLevel = (score: number) => {
  return LEVEL_SCORES.findIndex(v => v > score);
};

export const getLevelProgress = (score: number) => {
  const currentLevel = getCurrentLevel(score);
  const currentLevelMinScore = LEVEL_SCORES[currentLevel - 1];
  const nextLevelMinScore = LEVEL_SCORES[currentLevel];
  return (
    (score - currentLevelMinScore) / (nextLevelMinScore - currentLevelMinScore)
  );
};

export const addScore = (
  score: number,
  currentStatus: Game = { score: 0, level: 1, progress: 0 },
): Game => {
  const newScore = currentStatus.score + score;
  return {
    score: newScore,
    level: getCurrentLevel(newScore),
    progress: getLevelProgress(newScore),
  };
};

export const canAcceptMoreChallenge = (
  challenges: Challenge[],
  userLevel: number,
) => {
  const scores = challenges.map(c => getChallengeReward(c).complete);
  const rewardOfAll = scores.reduce((acc, cur) => acc + cur, 0);
  return rewardOfAll < LEVEL_SCORES[userLevel];
};
