import { TypegooseModule } from 'nestjs-typegoose';
import { TypegooseClass } from 'nestjs-typegoose/dist/typegoose-class.interface';

export const registerTypegooseModel = (modelClass: TypegooseClass) => {
  return TypegooseModule.forFeature([
    {
      typegooseClass: modelClass,
      schemaOptions: { timestamps: true },
    },
  ]);
};
