export enum Day {
  Sunday = 'sun',
  Monday = 'mon',
  Tuesday = 'tue',
  Wednesday = 'wen',
  Thursday = 'thu',
  Friday = 'fri',
  Saturday = 'sat',
}
