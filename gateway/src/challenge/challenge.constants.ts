export const validation = {
  name: {
    maxLength: 50,
  },
  goal: {
    maxLength: 200,
  },
  description: {
    maxLength: 800,
  },
  remindText: {
    maxLength: 200,
  },
  repeatCount: {
    max: 7,
  },
};
