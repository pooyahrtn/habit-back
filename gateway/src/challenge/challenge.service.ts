import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { Challenge, ChallengeModel } from './models/challenge.model';
import { CreateChallengeInput } from './dto/create-challenge.input';
import { UserDocumentType, User, Role } from '../users/models/user.model';
import { FilestorageService } from '../filestorage/filestorage.service';
import { DocumentQuery, FilterQuery, Types } from 'mongoose';
import { hasRole } from '../auth/utils';
import * as R from 'ramda';
import { GetChallengeInput } from './dto/get-challenge.input';
import { InlineExeption } from '../exeptions';
import { ChallengeDocuement } from '../challenge/models/challenge.model';
import { Task } from '../task/models/task.model';
import { ReportChallengeInput } from './dto/report-challenge.input';
import { ChallengeReport } from './models/challenge-report.model';
import { ConfirmChallengeInput } from './dto/confirm-challenge.input';
import { FirebaseService } from '../firebase/firebase.service';

@Injectable()
export class ChallengeService {
  constructor(
    @InjectModel(Challenge) private readonly challengeModel: ChallengeModel,
    private readonly filestorageService: FilestorageService,
    private readonly firebaseService: FirebaseService,
  ) {}

  async createChallenge(data: CreateChallengeInput, user: UserDocumentType) {
    const userIsTrusted =
      hasRole(user, [Role.ADMIN, Role.SUPER_ADMIN]) || user.game.level > 3;
    const challenge: Challenge = {
      ...data,
      user: user.id,
      startupChoice: false,
      confirmed: userIsTrusted,
      reports: [],
      stats: {
        enrolledCount: 0,
        successCount: 0,
      },
    };
    return await this.challengeModel.create(challenge);
  }

  async getChallenges(
    n: number,
    user: UserDocumentType,
    filter?: GetChallengeInput,
  ): Promise<Challenge[]> {
    const { count = 100, firstChallenges } = filter;
    const userChallenges = await this.getUserChallenges(user);

    return await R.pipe(
      sortByEnrolledCount,
      getTopResults(count),
      populateUser,
    )(
      this.challengeModel.find({
        confirmed: true,
        ...(userChallenges.length > 0 && {
          _id: {
            $nin: userChallenges,
          },
        }),
        ...(!!firstChallenges ? { startupChoice: firstChallenges } : {}),
      }),
    );
  }

  async findChallengeById(id: any): Promise<ChallengeDocuement> {
    const challenge = await this.challengeModel.findById(id);
    if (!challenge) {
      throw new InlineExeption('Challenge not found');
    }
    return challenge;
  }

  getChallengeImage(challenge: Challenge): string {
    return (
      this.filestorageService.challengeUrl() +
      challenge.category.toLowerCase() +
      '.png'
    );
  }

  // TODO: it's fuckedup
  async getUserChallenges(user: UserDocumentType): Promise<Types.ObjectId[]> {
    const userWithTasks = await user.populate('tasks').execPopulate();
    return userWithTasks.tasks
      .filter((t: Task) => t.active)
      .map((t: Task) => t.challenge as Types.ObjectId);
  }

  async reporttChallenge(user: UserDocumentType, input: ReportChallengeInput) {
    const { challengeId } = input;
    const challenge = await this.challengeModel.findById(challengeId);
    if (challenge) {
      challenge.reports.push({ user } as ChallengeReport);
    }
    await challenge.save();
    return true;
  }

  async getUnconfrimedChallenges(): Promise<Challenge[]> {
    return this.challengeModel
      .find({
        confirmed: false,
      })
      .populate('user');
  }

  async confirmChallenge(input: ConfirmChallengeInput): Promise<boolean> {
    const challenge = await this.challengeModel.findById(input.challengeId);
    challenge.confirmed = input.confirmed;
    challenge.category = input.category;
    await challenge.save();
    await challenge.populate('user').execPopulate();
    const user = challenge.user as User;
    if (user.settings?.fcmToken) {
      await this.firebaseService.sendNotification({
        userToken: user.settings.fcmToken,
        notification: {
          title: 'تایید چالش',
          body: `چالش ${challenge.name} تایید شد`,
        },
      });
    }
    return true;
  }
}

type ChallengesQuery = DocumentQuery<Challenge[], any>;

const populateUser = (q: ChallengesQuery) =>
  q.populate('user' as keyof Challenge, ['profile', 'id'] as Array<keyof User>);

const sortByEnrolledCount = (q: ChallengesQuery) =>
  q.sort('-stats.enrolledCount');
const getTopResults = (n: number) => (q: ChallengesQuery) => q.limit(n);
