import {
  Resolver,
  Mutation,
  Args,
  Query,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { Challenge } from './models/challenge.model';
import { AuthGaurd } from '../auth/auth.gaurd';
import { CreateChallengeInput } from './dto/create-challenge.input';
import { LevelGaurd } from '../users/gaurds/level.gaurd';
import { ChallengeService } from './challenge.service';
import { CurrentUser } from '../users/users.decorator';
import { UserDocumentType } from '../users/models/user.model';
import { GetChallengeInput } from './dto/get-challenge.input';
import { ReportChallengeInput } from './dto/report-challenge.input';

@Resolver(of => Challenge)
@AuthGaurd()
export class ChallengeResolver {
  constructor(private readonly challengeService: ChallengeService) {}

  @LevelGaurd(2)
  @Mutation(r => Challenge)
  async createChallenge(
    @Args('challenge') input: CreateChallengeInput,
    @CurrentUser() user: UserDocumentType,
  ): Promise<Challenge> {
    return this.challengeService.createChallenge(input, user);
  }

  @Query(r => [Challenge], { name: 'challenges' })
  async getChallenges(
    @CurrentUser() user: UserDocumentType,
    @Args('filter', { nullable: true }) filter?: GetChallengeInput,
  ): Promise<Challenge[]> {
    return await this.challengeService.getChallenges(3, user, filter);
  }

  @ResolveField('image', r => String)
  getChallengeImage(@Parent() challenge: Challenge) {
    return this.challengeService.getChallengeImage(challenge);
  }

  @Mutation(r => Boolean)
  async reportChallenge(
    @CurrentUser() user: UserDocumentType,
    @Args('input') input: ReportChallengeInput,
  ): Promise<boolean> {
    return this.challengeService.reporttChallenge(user, input);
  }
}
