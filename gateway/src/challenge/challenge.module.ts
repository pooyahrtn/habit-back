import { Module } from '@nestjs/common';
import { ChallengeService } from './challenge.service';
import { FilestorageModule } from '../filestorage/filestorage.module';
import { ChallengeResolver } from './challenge.resolver';
import { registerTypegooseModel } from '../utils';
import { Challenge } from './models/challenge.model';
import { FirebaseModule } from '../firebase/firebase.module';
import { ChallengeAdminResolver } from './challenge-admin.resolver';

@Module({
  imports: [
    registerTypegooseModel(Challenge),
    FilestorageModule,
    FirebaseModule,
  ],
  providers: [ChallengeService, ChallengeResolver, ChallengeAdminResolver],
  exports: [ChallengeService],
})
export class ChallengeModule {}
