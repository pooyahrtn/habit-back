import { registerEnumType } from '@nestjs/graphql';

export enum RemindTime {
  Any = 'Any',
  Morning = 'Morning',
  Afternoon = 'Afternoon',
  Evening = 'Evening',
  Night = 'Night',
}

registerEnumType(RemindTime, { name: 'RemindTime' });
