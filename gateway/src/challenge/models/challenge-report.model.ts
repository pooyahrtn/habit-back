import { prop, Ref } from '@typegoose/typegoose';
import { User } from '../../users/models/user.model';
import { registerEnumType } from '@nestjs/graphql';

export class ChallengeReport {
  @prop({ ref: 'User' })
  user: Ref<User>;
}
