import { registerEnumType } from '@nestjs/graphql';

export enum ChallengeCategory {
  Art = 'Art',
  Beauty = 'Beauty',
  Book = 'Book',
  Business = 'Business',
  Social = 'Social',
  Learn = 'Learn',
  Entertainment = 'Entertainment',
  Family = 'Family',
  Food = 'Food',
  Game = 'Game',
  Health = 'Health',
  Home = 'Home',
  Lifestyle = 'Lifestyle',
  Travel = 'Travel',
  Charity = 'Charity',
  Music = 'Music',
  Shopping = 'Shopping',
  Sport = 'Sport',
}

registerEnumType(ChallengeCategory, { name: 'ChallengeCategory' });
