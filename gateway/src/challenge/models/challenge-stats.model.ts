import { prop } from '@typegoose/typegoose';
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ChallengeStats {
  @Field(type => Int)
  @prop({ default: 0, index: true })
  enrolledCount: number;

  @Field(type => Int)
  @prop({ default: 0, index: true })
  successCount: number;
}
