import { validation } from '../challenge.constants';
import { Field, ObjectType, Int, ID } from '@nestjs/graphql';
import { prop, Ref, ReturnModelType, DocumentType, arrayProp } from '@typegoose/typegoose';
import { User } from '../../users/models/user.model';
import { Toughness } from './toughness.enum';
import { RemindTime } from './remind-time.enum';
import { ChallengeStats } from './challenge-stats.model';
import { Photo } from '../../photo/photo.model';
import { ChallengeCategory } from './challenge-category.enum';
import { ChallengeReport } from './challenge-report.model';

@ObjectType()
export class Challenge {
  @Field(t => ID)
  id?: string;

  @Field()
  @prop({ required: true, maxlength: validation.name.maxLength })
  name: string;

  @Field()
  @prop({ required: true, maxlength: validation.goal.maxLength })
  goal: string;

  @Field()
  @prop({ required: true, maxlength: validation.description.maxLength })
  description: string;

  @Field()
  @prop({ required: true, maxlength: validation.goal.maxLength })
  remindText: string;

  @Field(type => Int)
  @prop({ required: true, max: validation.repeatCount.max })
  repeatCount: number;

  @Field(type => User)
  @prop({ ref: 'User' })
  user: Ref<User>;

  @Field(type => Toughness)
  @prop({ enum: Toughness })
  toughness: Toughness;

  @Field(t => RemindTime)
  @prop({ enum: RemindTime })
  remindTime: RemindTime;

  @Field(type => ChallengeStats)
  @prop({ _id: false })
  stats: ChallengeStats;

  @Field(t => ChallengeCategory)
  @prop({ enum: ChallengeCategory })
  category: ChallengeCategory;

  @Field()
  image?: string;

  @prop()
  startupChoice: boolean;

  @Field()
  @prop({ default: false })
  confirmed: boolean;

  @arrayProp({ items: ChallengeReport })
  reports: ChallengeReport[];
}

export type ChallengeModel = ReturnModelType<typeof Challenge>;
export type ChallengeDocuement = DocumentType<Challenge>;
