import { registerEnumType } from '@nestjs/graphql';

export enum Toughness {
  Easy = 'Easy',
  Medium = 'Medium',
  Hard = 'Hard',
}

registerEnumType(Toughness, { name: 'Toughness' });
