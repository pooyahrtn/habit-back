import {
  Resolver,
  Mutation,
  Args,
  Query,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { Challenge } from './models/challenge.model';
import { AuthGaurd } from '../auth/auth.gaurd';
import { ChallengeService } from './challenge.service';
import { CurrentUser } from '../users/users.decorator';
import { UserDocumentType, Role } from '../users/models/user.model';
import { GetChallengeInput } from './dto/get-challenge.input';
import { ConfirmChallengeInput } from './dto/confirm-challenge.input';

@Resolver(of => Challenge)
@AuthGaurd([Role.ADMIN, Role.SUPER_ADMIN])
export class ChallengeAdminResolver {
  constructor(private readonly challengeService: ChallengeService) {}

  @Query(r => [Challenge], { name: 'unconfirmedChallenges' })
  async getUnconfirmedChallenges(
    @CurrentUser() user: UserDocumentType,
    @Args('filter', { nullable: true }) filter?: GetChallengeInput,
  ): Promise<Challenge[]> {
    return await this.challengeService.getUnconfrimedChallenges();
  }

  @Mutation(r => Boolean)
  async confirmChallenge(
    @Args('input') input: ConfirmChallengeInput,
  ): Promise<boolean> {
    return this.challengeService.confirmChallenge(input);
  }
}
