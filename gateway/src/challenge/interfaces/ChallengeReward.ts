
export interface ChallengeReward {
  step: number;
  complete: number;
}
