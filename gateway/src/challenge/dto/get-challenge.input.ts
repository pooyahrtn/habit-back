import { Field, Int, InputType } from '@nestjs/graphql';
import { Max } from 'class-validator';
import { Toughness } from '../models/toughness.enum';

@InputType()
export class GetChallengeInput {
  @Field(t => Toughness, { nullable: true })
  toughness?: Toughness;

  @Field(t => Int, { nullable: true })
  count?: number;

  @Field({ nullable: true })
  trending?: boolean;

  @Field({ nullable: true })
  firstChallenges?: boolean;
}
