import { InputType, Field, ID } from '@nestjs/graphql';
import { ChallengeCategory } from '../models/challenge-category.enum';

@InputType()
export class ConfirmChallengeInput {
  @Field(t => ID)
  challengeId: string;

  @Field()
  confirmed: boolean;

  @Field(t => ChallengeCategory)
  category: ChallengeCategory;
}
