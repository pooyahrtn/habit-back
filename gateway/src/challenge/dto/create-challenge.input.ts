import { Field, Int, InputType } from '@nestjs/graphql';
import { Length, Min, Max } from 'class-validator';
import { validation } from '../challenge.constants';
import { Toughness } from '../models/toughness.enum';
import { RemindTime } from '../models/remind-time.enum';
import { ChallengeCategory } from '../../challenge/models/challenge-category.enum';

@InputType()
export class CreateChallengeInput {
  @Field()
  @Length(1, validation.name.maxLength)
  name: string;

  @Field()
  @Length(10, validation.goal.maxLength)
  goal: string;

  @Field()
  @Length(10, validation.description.maxLength)
  description: string;

  @Field(type => Int)
  @Min(1)
  @Max(validation.repeatCount.max)
  repeatCount: number;

  @Field(t => Toughness)
  toughness: Toughness;

  @Field(t => RemindTime)
  remindTime: RemindTime;

  @Field()
  @Length(5, validation.remindText.maxLength)
  remindText: string;

  @Field(t => ChallengeCategory)
  category: ChallengeCategory;

}
