import { InputType, Field, ID } from '@nestjs/graphql';

@InputType()
export class ReportChallengeInput {
  @Field(t => ID)
  challengeId: string;
}
